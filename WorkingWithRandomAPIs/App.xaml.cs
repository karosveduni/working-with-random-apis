﻿using System;
using System.Windows;

using Microsoft.Extensions.DependencyInjection;

using WorkingWithRandomAPIs.View;
using WorkingWithRandomAPIs.ViewModels;

namespace WorkingWithRandomAPIs
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
        public App()
        {
			Services = ConfigureServices();
			MainWindow = Services.GetService<MainWindow>();
			MainWindow!.Show();
		}

		/// <summary>
		/// Gets the current <see cref="App"/> instance in use
		/// </summary>
		public new static App Current => (App)Application.Current;

		/// <summary>
		/// Gets the <see cref="IServiceProvider"/> instance to resolve application services.
		/// </summary>
		public IServiceProvider Services { get; private set; }

		/// <summary>
		/// Configures the services for the application.
		/// </summary>
		private static IServiceProvider ConfigureServices()
		{
			ServiceCollection services = new ServiceCollection();
			
			services.AddSingleton<MainWindow>();
			services.AddSingleton<MainWindowViewModel>();

			services.AddSingleton<ChuckNorrisUserControl>();
			services.AddSingleton<ChuckNorrisViewModel>();

			services.AddSingleton<PicsumPhotosUserControl>();
			services.AddSingleton<PicsumPhotosViewModel>();

			services.AddSingleton<RandomUserUserControl>();
			services.AddSingleton<RandomUserViewModel>();

			return services.BuildServiceProvider();
		}
	}
}
