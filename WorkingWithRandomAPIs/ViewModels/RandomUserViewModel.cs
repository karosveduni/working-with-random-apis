﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;

using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Polly;
using Polly.Retry;

using Refit;

using WorkingWithRandomAPIs.Interfaces;
using WorkingWithRandomAPIs.Models;

namespace WorkingWithRandomAPIs.ViewModels
{
	public partial class RandomUserViewModel : BaseViewModel
	{
		private readonly RetryPolicy _retryPolicy;
		private readonly IRandomUserAPI _randomUserAPI;

		[ObservableProperty]
		private User _user;

		public RandomUserViewModel()
		{
			_randomUserAPI = RestService.For<IRandomUserAPI>("https://randomuser.me/");
			_retryPolicy = RetryPolicy.Handle<Exception>().Retry(3, (ex, count) => { MessageBox.Show(ex.Message); });
		}

		public override Task LoadAsync()
		{
			throw new System.NotImplementedException();
		}

		[RelayCommand]
		private async Task GetRandomUserAsync()
		{
			try
			{
				User = await _retryPolicy.Execute(_randomUserAPI.GetRandomUser);

				if (User is null)
				{
					MessageBox.Show("Проблема с Интернетом или API. Данные не получены.", "Ошибка при получение даных.");
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show($"Ошибка: {ex.Message}. Нажимайте еще раз.", "Ошибка");
			}
		}
	}
}
