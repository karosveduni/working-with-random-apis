﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Polly;
using Polly.Retry;

using Refit;

using WorkingWithRandomAPIs.Interfaces;
using WorkingWithRandomAPIs.Models;

namespace WorkingWithRandomAPIs.ViewModels
{
	public partial class ChuckNorrisViewModel : BaseViewModel
	{
		private IChuckNorrisAPI _chuckNorrisAPI;
		private RetryPolicy _retryPolicy;

		[ObservableProperty]
		private ChuckNorris _chuckNorris;

		[ObservableProperty]
		private ObservableCollection<string> _categories;

		[ObservableProperty]
		private string _selectedCategory;

		[ObservableProperty]
		private bool _isLoadData;

		[ObservableProperty]
		private bool _isEnabledButton = false;

		[ObservableProperty]
		private string _query;

		public ChuckNorrisViewModel()
		{
			_chuckNorrisAPI = RestService.For<IChuckNorrisAPI>("https://api.chucknorris.io");
			_retryPolicy = RetryPolicy
				.Handle<HttpRequestException>()
				.Retry(3, OnRetry);
		}

		private void OnRetry(Exception ex, int count)
		{
			MessageBox.Show(ex.Message, "Error");
		}

		public override async Task LoadAsync()
		{
			IsLoadData = true;
			var _categories = await _chuckNorrisAPI.GetCategories();
			Categories = new ObservableCollection<string>(_categories)
			{
				string.Empty
			};
			SelectedCategory = Categories[^1];
			IsEnabledButton = true;
			IsLoadData = false;
		}

		[RelayCommand]
		private Task GetChuckNorris()
		{
			return _retryPolicy.Execute<Task>(async () =>
			{
				if (SelectedCategory is null or { Length: 0 })
				{
					if (Query is null or { Length: 0 })
					{
						ChuckNorris = await _chuckNorrisAPI.GetRandom();
					}
					else
					{
						ChuckNorris = await _chuckNorrisAPI.GetChuckHorrisWithQuery(Query);
					}
				}
				else
				{
					ChuckNorris = await _chuckNorrisAPI.GetRandomWithCategory(SelectedCategory);
				}
			});

		}
	}
}
