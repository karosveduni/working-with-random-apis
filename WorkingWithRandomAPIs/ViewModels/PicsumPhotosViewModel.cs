﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Refit;

using WorkingWithRandomAPIs.Interfaces;
using WorkingWithRandomAPIs.Models;

namespace WorkingWithRandomAPIs.ViewModels
{
	public partial class PicsumPhotosViewModel : BaseViewModel
	{
		private const int WIDTH = 300;
		private const int HEIGHT = 200;

		private readonly IPicsumPhotosAPI _photosAPI;
		private PicsumPhotosModel _picsumPhotosModel;

		[ObservableProperty]
		private ImageSource _imageSource;

		[ObservableProperty]
		private string _textId;
		[ObservableProperty]
		private string _textSeed;

		public PicsumPhotosViewModel()
		{
			_photosAPI = RestService.For<IPicsumPhotosAPI>("https://picsum.photos/");
		}

		public override async Task LoadAsync()
		{
			await GetImageWithWidthAndHeightAsync();
		}

		[RelayCommand]
		private async Task GetImageWithWidthAndHeightAsync()
		{
			Stream stream = await _photosAPI.GetStreamForImage(WIDTH, HEIGHT);
			ImageSource = ConvertStreamToImageSource(stream);
		}

		[RelayCommand]
		private async Task GetImageWithSizeAsync()
		{
			Stream stream = await _photosAPI.GetStreamForImage(150);
			ImageSource = ConvertStreamToImageSource(stream);
		}

		[RelayCommand]
		private async Task GetImageWithIdAsync()
		{
			Stream stream = await _photosAPI.GetStreamForImage(int.Parse(TextId), WIDTH, HEIGHT);
			ImageSource = ConvertStreamToImageSource(stream);
		}

		[RelayCommand]
		private async Task GetImageWithSeedAsync()
		{
			Stream stream = await _photosAPI.GetStreamWithSeed(int.Parse(TextSeed), WIDTH, HEIGHT);
			ImageSource = ConvertStreamToImageSource(stream);
		}

		[RelayCommand]
		private async Task GetImageWithGrayscaleAsync()
		{
			Stream stream = await _photosAPI.GetStreamForImageWithGrayscale(WIDTH, HEIGHT);
			ImageSource = ConvertStreamToImageSource(stream);
		}

		/// <summary>
		/// Сгенерировал Искусственный интеллект
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public ImageSource ConvertStreamToImageSource(Stream stream)
		{
			try
			{
				BitmapImage imageSource = new BitmapImage();
				imageSource.BeginInit();
				imageSource.CacheOption = BitmapCacheOption.OnLoad;
				imageSource.StreamSource = stream;
				imageSource.EndInit();

				return imageSource;
			}
			catch (Exception ex)
			{
				// Обработка ошибок, если возникают проблемы при конвертации потока данных
				MessageBox.Show("Ошибка конвертации в ImageSource: " + ex.Message);
				return null;
			}
		}

	}
}
