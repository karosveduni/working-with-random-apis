﻿using System.Threading.Tasks;

using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Microsoft.Extensions.DependencyInjection;

using WorkingWithRandomAPIs.View;

namespace WorkingWithRandomAPIs.ViewModels
{
	public partial class MainWindowViewModel : BaseViewModel
	{
		[ObservableProperty]
		private object? _content;

        public MainWindowViewModel()
        {
			TabChuckNorris();
        }

		public override Task LoadAsync()
		{
			throw new System.NotImplementedException();
		}

		[RelayCommand]
		private void TabChuckNorris()
		{
			Content = App.Current.Services.GetService<ChuckNorrisUserControl>();
		}

		[RelayCommand]
		private void TabPicsumPhotos()
		{
			Content = App.Current.Services.GetService<PicsumPhotosUserControl>();
		}

		[RelayCommand]
		private void TabRandomUser()
		{
			Content = App.Current.Services.GetService<RandomUserUserControl>();
		}
	}
}
