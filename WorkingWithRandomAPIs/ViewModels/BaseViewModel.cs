﻿using System.Threading.Tasks;

using CommunityToolkit.Mvvm.ComponentModel;

using WorkingWithRandomAPIs.Interfaces;

namespace WorkingWithRandomAPIs.ViewModels
{
	public abstract class BaseViewModel : ObservableObject, ILoad
	{
		public abstract Task LoadAsync();
	}
}
