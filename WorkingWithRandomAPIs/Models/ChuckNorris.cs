﻿namespace WorkingWithRandomAPIs.Models
{
	public class ChuckNorris
	{
		public string? Id { get; set; }
		public string? Url { get; set; }
		public string? Value { get; set; }
	}
}
