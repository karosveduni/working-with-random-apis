﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Refit;

using WorkingWithRandomAPIs.Models;

namespace WorkingWithRandomAPIs.Interfaces
{
	public interface IChuckNorrisAPI
	{
		[Get("/jokes/random")]
		Task<ChuckNorris> GetRandom();

		[Get("/jokes/random?category={category}")]
		Task<ChuckNorris> GetRandomWithCategory(string category);

		[Get("/jokes/categories")]
		Task<List<string>> GetCategories();

		[Get("/jokes/search?query={query}")]
		Task<ChuckNorris> GetChuckHorrisWithQuery(string query);
	}
}
