﻿using System.Threading.Tasks;

namespace WorkingWithRandomAPIs.Interfaces
{
	public interface ILoad
	{
		Task LoadAsync();
	}
}
