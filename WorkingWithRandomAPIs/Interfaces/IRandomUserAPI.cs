﻿using System.Threading.Tasks;

using Refit;

using WorkingWithRandomAPIs.Models;

namespace WorkingWithRandomAPIs.Interfaces
{
	public interface IRandomUserAPI
	{
		[Get("/api/")]
		Task<User> GetRandomUser();
	}
}




