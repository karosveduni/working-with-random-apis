﻿using System.IO;
using System.Threading.Tasks;

using Refit;

namespace WorkingWithRandomAPIs.Interfaces
{
	public interface IPicsumPhotosAPI
	{
		[Get("/{width}/{height}")]
		Task<Stream> GetStreamForImage(int width, int height);

		[Get("/{size}")]
		Task<Stream> GetStreamForImage(int size);

		[Get("/id/{id}/{width}/{height}")]
		Task<Stream> GetStreamForImage(int id, int width, int height);

		[Get("/seed/{seed}/{width}/{height}")]
		Task<Stream> GetStreamWithSeed(int seed, int width, int height);

		[Get("/{width}/{height}?grayscale")]
		Task<Stream> GetStreamForImageWithGrayscale(int width, int height);
	}
}
