﻿using System;
using System.Windows.Controls;

using WorkingWithRandomAPIs.Interfaces;
using WorkingWithRandomAPIs.ViewModels;

namespace WorkingWithRandomAPIs.View
{
	/// <summary>
	/// Логика взаимодействия для ChuckNorrisPage.xaml
	/// </summary>
	public partial class ChuckNorrisUserControl : UserControl
	{
		public ChuckNorrisUserControl(ChuckNorrisViewModel vm)
		{
			DataContext = vm;
			InitializeComponent();
		}

		protected override async void OnInitialized(EventArgs e)
		{
			base.OnInitialized(e);
			if(DataContext is ILoad load)
			{
				await load.LoadAsync();
			}
		}
	}
}
