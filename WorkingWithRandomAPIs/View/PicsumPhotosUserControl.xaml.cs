﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

using WorkingWithRandomAPIs.Interfaces;
using WorkingWithRandomAPIs.ViewModels;

namespace WorkingWithRandomAPIs.View
{
	/// <summary>
	/// Логика взаимодействия для PicsumPhotosPage.xaml
	/// </summary>
	public partial class PicsumPhotosUserControl : UserControl
	{
		public PicsumPhotosUserControl(PicsumPhotosViewModel vm)
		{
			DataContext = vm;
			InitializeComponent();
		}

		protected override async void OnInitialized(EventArgs e)
		{
			base.OnInitialized(e);
			if (DataContext is ILoad load)
			{
				await load.LoadAsync();
			}
		}

		private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			// Получаем текущий текст в TextBox
			string currentText = ((TextBox)sender).Text;

			// Формируем строку, которая будет получена после добавления вводимого текста
			string newText = currentText.Insert(((TextBox)sender).SelectionStart, e.Text);

			// Проверяем, является ли вводимый текст числом
			if (!int.TryParse(newText, out int inputNumber))
			{
				e.Handled = true; // Отменяем ввод, если введенный текст не является числом
			}
			else
			{
				// Если введенное число меньше 1, отменяем ввод
				if (inputNumber < 1)
				{
					e.Handled = true;
				}
			}
		}
	}
}
