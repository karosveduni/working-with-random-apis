﻿using System.Windows;

using WorkingWithRandomAPIs.ViewModels;

namespace WorkingWithRandomAPIs.View
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow(MainWindowViewModel vm)
		{
			InitializeComponent();
			DataContext = vm;
		}
    }
}
