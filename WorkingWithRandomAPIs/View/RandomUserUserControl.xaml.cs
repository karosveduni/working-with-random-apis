﻿using System.Windows.Controls;

using WorkingWithRandomAPIs.ViewModels;

namespace WorkingWithRandomAPIs.View
{
	/// <summary>
	/// Логика взаимодействия для RandomUserPage.xaml
	/// </summary>
	public partial class RandomUserUserControl : UserControl
	{
		public RandomUserUserControl(RandomUserViewModel vm)
		{
			DataContext = vm;
			InitializeComponent();
		}
	}
}
